USE StoredPr_DB; 

#drop procedure insertIntoEmployee;

DELIMITER //
CREATE PROCEDURE insertIntoEmployee(
	IN id int(11),
	IN surname varchar(30),
	IN name char(30),
	IN middle_name varchar(30),
	IN identity_number char(10),
	IN passport char(10),
	IN  experience decimal(10,1),
	IN birthday date,
	IN post varchar(15),
	IN pharmacy_id int(11))
BEGIN
	INSERT INTO `storedpr_db`.`employee` (`id`, `surname`, `name`, `midle_name`, `identity_number`, `passport`, `experience`, `birthday`, `post`, `pharmacy_id`) 
    VALUES (id, surname, name, middle_name, identity_number, passport, experience, birthday, post, pharmacy_id);
END //
DELIMITER ;


#drop procedure insertIntoMedicineZone;
DELIMITER //
CREATE PROCEDURE insertIntoMedicineZone(
	IN medicine_id int(11),
    IN zone_id int(11))
BEGIN
	INSERT INTO medicine_zone
    VALUES (medicine_id ,  zone_id);
END //
DELIMITER ;

drop procedure ProcCursor;
DELIMITER //
CREATE PROCEDURE ProcCursor()
BEGIN
DECLARE done int DEFAULT false;
DECLARE NameTable char(25);
DECLARE new_column char (30);
DECLARE x int;
DECLARE random_col decimal(2,0);
DECLARE Emp_Cursor CURSOR
FOR SELECT name FROM employee;
DECLARE CONTINUE HANDLER
FOR NOT FOUND SET done = true;
OPEN Emp_Cursor;
myLoop: LOOP
	FETCH Emp_Cursor INTO NameTable;
	IF done=true THEN LEAVE myLoop;
	END IF;
	SET random_col = (SELECT RAND()*(10-1)+1), x=1;
	SET @temp_query=CONCAT('CREATE TABLE ', NameTable, random_col,'(');
		generateColumn: LOOP
			IF x > random_col THEN LEAVE generateColumn;
			END IF;
			SET new_column = CONCAT('column', x, ' varchar(15)');
			IF (x+1) <= random_col THEN SET @temp_query = CONCAT(@temp_query,new_column,', ');
			ELSEIF (x+1) > random_col THEN SET @temp_query = CONCAT(@temp_query,new_column,');');
			END IF;
			SET x = x + 1;
			ITERATE generateColumn;
		END LOOP;
	PREPARE myquery FROM @temp_query;
	EXECUTE myquery;
	DEALLOCATE PREPARE myquery;
END LOOP;
CLOSE Emp_Cursor;
END //
DELIMITER ;
