USE StoredPr_DB; 

#SELECT Concat('DROP TRIGGER ', Trigger_Name, ';') FROM  information_schema.TRIGGERS WHERE TRIGGER_SCHEMA = 'storedpr_db';
#SHOW TRIGGERS from storedpr_db;

CREATE TABLE IF NOT EXISTS error_msg (error_msg VARCHAR(32) NOT NULL PRIMARY KEY);
INSERT INTO error_msg VALUES ('Foreign Key Constraint Violated!');
INSERT INTO error_msg VALUES ('Forbiden!');

#Тригер на insert employee
DELIMITER //
CREATE TRIGGER insert_employee
  BEFORE INSERT
  ON employee
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM post WHERE post=new.post)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF new.identity_number rlike '.+00$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
 END //
DELIMITER ;

#Тригер на update employee
DELIMITER //
CREATE TRIGGER update_employee
  BEFORE UPDATE
  ON employee
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM post WHERE post=new.post)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF new.identity_number rlike '.+00$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER insert_pharmacy
  BEFORE INSERT
  ON pharmacy
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM street WHERE street=new.street)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#Тригер на update pharmacy
DELIMITER //
CREATE TRIGGER update_pharmacy
  BEFORE UPDATE
  ON pharmacy
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM street WHERE street=new.street)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на after update pharmacy
DELIMITER //
CREATE TRIGGER update_after_pharmacy
  AFTER UPDATE
  ON pharmacy
  FOR EACH ROW
  BEGIN
    IF new.id != old.id 
    THEN 
		UPDATE pharmacy_medicine SET pharmacy_id=new.id WHERE 
		pharmacy_id=old.id;
        UPDATE employee SET pharmacy_id=new.id WHERE 
		pharmacy_id=old.id;
	END IF;
  END //
DELIMITER ;

#Тригер на insert medicine
DELIMITER //
CREATE TRIGGER insert_medicine
  BEFORE INSERT
  ON medicine
  FOR EACH ROW
  BEGIN
    IF new.ministry_code not rlike '^[^МП]{2}-[0-9]{3}-[0-9]{2}$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#Тригер на update medicine
DELIMITER //
CREATE TRIGGER update_medicine
  BEFORE UPDATE
  ON medicine
  FOR EACH ROW
  BEGIN
    IF new.ministry_code not rlike '^[^МП]{2}-[0-9]{3}-[0-9]{2}$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на insert pharmacy_medicine
DELIMITER //
CREATE TRIGGER insert_pharmacy_medicine
  BEFORE INSERT
  ON pharmacy_medicine
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
    IF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#Тригер на update pharmacy_medicine
DELIMITER //
CREATE TRIGGER update_pharmacy_medicine
  BEFORE UPDATE
  ON pharmacy_medicine
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на insert medicine_zone
DELIMITER //
CREATE TRIGGER insert_medicine_zone
  BEFORE INSERT
  ON medicine_zone
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM zone WHERE id=new.zone_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#Тригер на update medicine_zone
DELIMITER //
CREATE TRIGGER update_medicine_zone
  BEFORE UPDATE
  ON medicine_zone
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM zone WHERE id=new.zone_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#Тригер на after update medicine
DELIMITER //
CREATE TRIGGER update_after_medicine
  AFTER UPDATE
  ON medicine
  FOR EACH ROW
  BEGIN
    IF new.id != old.id 
    THEN 
		UPDATE pharmacy_medicine SET medicine_id=new.id WHERE 
		medicine_id=old.id;
        UPDATE medicine_zone SET medicine_id=new.id WHERE 
		medicine_id=old.id;
	END IF;
  END //
DELIMITER ;

#Тригер на видалення з post
DELIMITER //
CREATE TRIGGER delete_post
  BEFORE DELETE
  ON post
  FOR EACH ROW
  BEGIN
    INSERT error_msg VALUES ('Forbiden!');
  END //
DELIMITER ;

#Тригер на видалення з street
DELIMITER //
CREATE TRIGGER delete_street
  BEFORE DELETE
  ON street
  FOR EACH ROW
  BEGIN
      INSERT error_msg VALUES ('Forbiden!');
  END //
DELIMITER ;

#Тригер на видалення з zone
DELIMITER //
CREATE TRIGGER delete_zone
  BEFORE DELETE
  ON zone
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM medicine_zone WHERE zone_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на видалення з pharmacy
DELIMITER //
CREATE TRIGGER delete_pharmacy
  BEFORE DELETE
  ON pharmacy
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM employee WHERE pharmacy_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF(SELECT COUNT(*) FROM pharmacy_medicine WHERE pharmacy_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на видалення з medicine
DELIMITER //
CREATE TRIGGER delete_medicine
  BEFORE DELETE
  ON medicine
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM medicine_zone WHERE medicine_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF(SELECT COUNT(*) FROM pharmacy_medicine WHERE medicine_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#Тригер на update з post
DELIMITER //
CREATE TRIGGER update_post
  BEFORE UPDATE
  ON post
  FOR EACH ROW
  BEGIN
    INSERT error_msg VALUES ('Forbiden!');
  END //
DELIMITER ;

