USE StoredPr_DB; 

drop function MinExperience;
DELIMITER //
CREATE FUNCTION MinExperience()
RETURNS DECIMAL(10,1)
READS SQL DATA
BEGIN
	RETURN (SELECT min(experience) from employee);
END //
DELIMITER ;

drop function GetNameAndPharmacyNumber;
DELIMITER //
CREATE FUNCTION GetNameAndPharmacyNumber(pharmacy_id int(11))
RETURNS varchar(25)
READS SQL DATA
BEGIN
	RETURN (SELECT CONCAT_WS('', name, building_number) FROM pharmacy where id = pharmacy_id);
END //
DELIMITER ;

